import { Component, OnInit } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';
import { Service } from './service';

@Component({
	selector: 'header',
	templateUrl: "../assets/header.html",
	styles: [`
.dropbtn {
    background-color: #ffa21d;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #ffa21d;
}`],
  	directives: [ROUTER_DIRECTIVES]	

})	

export class Header implements OnInit{

public res;
public error;
constructor(private router: Router,private service:Service) {

}

ngOnInit() {
	// this.service.list = false;
}

logout() {
	localStorage.removeItem('user');
	this.service.get('logout')
	.then((res) => {
		this.res = res;
		this.service.list = false;
	})
	.catch(error => this.error = error);
	this.router.navigate(['Login']);

}

}