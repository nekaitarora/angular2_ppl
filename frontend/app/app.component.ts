import { Component,ViewContainerRef }       from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, AsyncRoute } from '@angular/router-deprecated';
import {loginComponent} from './login.component';
import {dashboardComponent} from './dashboard.component';
import {Header} from './header';
import {Footer} from './footer';
import {signupComponent} from './signup.component';
import {profileComponent} from './profile.component';
import {singlePostComponent} from './singlePost.component';
import {forgotComponent} from './forgot.component';
import {changePasswordComponent} from './changePassword.component';
import { Service } from './service';
import {verifyUserComponent} from './verifyUser.component';
import {resetPasswordComponent} from './resetPassword.component';
import { dashboardCategoryComponent } from './dashboardCategory.component';
import {Modal, BS_MODAL_PROVIDERS} from 'angular2-modal/plugins/bootstrap';

declare var System:any;

@Component({
	selector: 'my-app',
	template: `
	<header></header><router-outlet></router-outlet><footer></footer>
	`,
  	directives: [ROUTER_DIRECTIVES,Footer,Header]	,
	providers: [Service,BS_MODAL_PROVIDERS]
})



@RouteConfig([
  {
    path: '/login',
    name: 'Login',
    component: loginComponent
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: dashboardComponent,
      useAsDefault: true  
  },
  {
    path: '/signup',
    name: 'Signup',
    component: signupComponent
  },
  {
    path: '/profile',
    name: 'Profile',
    component: profileComponent
  },
  {
    path: '/singlepost/:id',
    name: 'SinglePost',
    component: singlePostComponent
  },
  {
    path: '/forgot',
    name: 'Forgot',
    component: forgotComponent
  },
  {
    path: '/changepassword',
    name: 'ChangePassword',
    component: changePasswordComponent
  },
  {
    path: '/user/verify/:token/:id',
    name: 'VerifyUser',
    component: verifyUserComponent
  },
  {
    path: '/resetpassword/:token/:id',
    name: 'ResetPassword',
    component: resetPasswordComponent
  },
  {
    path: '/dashboard/:category',
    name: 'DashboardCategory',
    component: dashboardCategoryComponent
  }               
])


export class AppComponent {
	title = 'ppl loading';

  constructor(public modal: Modal, viewContainer: ViewContainerRef) {
    modal.defaultViewContainer = viewContainer;
  }
}