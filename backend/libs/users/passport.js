//use statergy
var LocalStrategy = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    users_db = require('./users_db.js');

var hash = new users_db();
module.exports = function(passport) {
  passport.serializeUser(function(user, done) {
    console.log('serializeUser',user.id);
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        console.log('deserializeUser',id);
        users_db.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, username, password, done) {
        process.nextTick(function() {
         
            users_db.findOne({ email: username}).exec(function(err, user) {

                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, "No User Found" );
                }
                if (!user.validPassword(password)) {
                    return done(null, false, "Wrong Password" );
                }
                if(user.approved == false){
                    return done(null, false, "Email Verification Pending");
                }else {
                    return done(null, user);
                }
            });
        });
    }
));

}
