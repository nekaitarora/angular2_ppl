var express = require("express");
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var fs = require('file-system');
var busboy = require('connect-busboy')
var cors = require('express-cors');
var passport = require('passport');
var session   = require('express-session');
var FacebookStrategy = require('passport-facebook').Strategy;
const MongoStore = require('connect-mongo')(session);
var mail = require('./config/mail.js');
var path = require('path');

var cookieParser = require('cookie-parser');
mongoose.connect('mongodb://localhost/ppl');

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(busboy());
app.use(busboy({immediate:false}));

app.use(cors({
  allowedOrigins: [
    'http://139.162.5.142:3002','angular2.daffodilapps.com:3002','http://139.162.5.142:3000','http://139.162.5.142:3001','angular2.daffodilapps.com:3000','angular2.daffodilapps.com:3001','localhost:3000','localhost:3001','localhost:3002','localhost:3003'
  ],
  credentials: true
}))
app.use(express.static(__dirname + '/apidoc'));

// app.use(express.static(__dirname + '/../frontend'));
app.use(express.static(__dirname + '/public'));

app.get('/apidoc',function(req, res){
    res.sendFile(path.join(__dirname, '/apidoc', 'index.html'));
});


app.use(session({
   secret: 'secret',
   resave: false,
 saveUninitialized: true,
 cookie: {  
   path     : '/',  
   domain   : ['139.162.5.142','angular2.daffodilapps.com'],  
   httpOnly : true,  
   maxAge   : 1000*60*60*24*30*12    //one year(ish)  
 },
   store: new MongoStore({
       url: 'mongodb://localhost:27017/ppl',
   })
}));

app.use(passport.initialize());
app.use(passport.session());

require('./libs/users/passport')(passport);
require('./routes/index')(app);
require('./routes/upload')(app,__dirname);

// app.get('/flagbx',function(req,res){
// 	db.post.find({"flagBy":{$exists:true,$ne:[]}},function(err,data){
// 		if(err) throw err;
// 		else{
// 			res.json(data);
// 		}
// 	})
// })

app.listen(4000,function(){
	console.log('connection successful');
})

module.exports = app;
