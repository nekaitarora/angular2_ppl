

var categories_db = require('./categories_db');

module.exports = {

	getCategories: function(cb) {
		categories_db.find({},function(err,result){
			if(err) {return cb(err,null);}
			return cb(null,result);
		})
	},

    createCategory: function(category,cb) {
    	var newCategory = new categories_db(category);
    	newCategory.save(function(err,result){
    		if(err) { return cb(err,null); }
    		return cb(null,result);
    	})
    },

    deleteCategory: function(id,cb) {
    	categories_db.remove({"_id":id}).exec(function(err,result){
    		if(err) {return cb(err,null);}
    		return cb(null,result);
    	})
    },

    updateCategory: function(id,data,cb) {
    	categories_db.findOneAndUpdate({"_id": id},{$set:data}).exec(function(err,result){
    		if(err) {return cb(err,null);}
    		return cb(null,result);
    	})
    }

}