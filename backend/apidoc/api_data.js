define({ "api": [
  {
    "type": "post",
    "url": "addCategories",
    "title": "Add new category",
    "name": "AddCategory",
    "group": "Category",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571dd4a28d901ebd1ab99221\"),\n\t\t\"img\" : \"/images/Img-2351696246769279.jpeg\",\n\t\t\"categories\" : \"quotes\",\n\t\t\"__v\" : 0\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No post found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "categories",
    "title": "Get all categories",
    "name": "AllCategories",
    "group": "Category",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571dd4a28d901ebd1ab99221\"),\n\t\t\"img\" : \"/images/Img-2351696246769279.jpeg\",\n\t\t\"categories\" : \"quotes\",\n\t\t\"__v\" : 0\n\t}\n\t{\n\t\t\"_id\" : ObjectId(\"571dd5ab8d901ebd1ab99222\"),\n\t\t\"img\" : \"/images/Img-4672146360389888.jpeg\",\n\t\t\"categories\" : \"wallpapers\",\n\t\t\"__v\" : 0\n\t}\n\t{\n\t\t\"_id\" : ObjectId(\"57776144f5f7eeed19749031\"),\n\t\t\"img\" : \"/images/Img-929281897842884.jpeg\",\n\t\t\"categories\" : \"london\",\n\t\t\"__v\" : 0\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No Category found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "Category"
  },
  {
    "type": "delete",
    "url": "deleteCategories/:id",
    "title": "Delete category",
    "name": "DeleteCategory",
    "group": "Category",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571dd4a28d901ebd1ab99221\"),\n\t\t\"img\" : \"/images/Img-2351696246769279.jpeg\",\n\t\t\"categories\" : \"quotes\",\n\t\t\"__v\" : 0\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No category found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "Category"
  },
  {
    "type": "put",
    "url": "updatecat/:id",
    "title": "Update category",
    "name": "UpdateCategory",
    "group": "Category",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>category type.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>category Image.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571dd4a28d901ebd1ab99221\"),\n\t\t\"img\" : \"/images/Img-2351696246769279.jpeg\",\n\t\t\"categories\" : \"quotes\",\n\t\t\"__v\" : 0\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No category found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "getComment/:id",
    "title": "Get comment for post",
    "name": "CommentForPost",
    "group": "Comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Posts unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"577769c8bcdab8a5223c6395\"),\n\t\t\"createdOn\" : ISODate(\"2016-07-02T07:14:16Z\"),\n\t\t\"postedBy\" : \"akshay.rao@daffodilsw.com\",\n\t\t\"catType\" : \"quotes\",\n\t\t\"img\" : \"/images/Img-1661310859490186.jpeg\",\n\t\t\"title\" : \"hello\",\n\t\t\"user_id\" : \"57711864bb9456b373f0956b\",\n\t\t\"featured\" : true,\n\t\t\"flagBy\" : [\n\t\t\t\"57711864bb9456b373f0956b\"\n\t\t],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 0,\n\t\t\"likeBy\" : [ ],\n\t\t\"commentCount\" : 1,\n\t\t\"comments\" : [\n\t\t\t{\n\t\t\t\t\"commentedOn\" : \"577769c8bcdab8a5223c6395\",\n\t\t\t\t\"comment\" : \"Istanbul :)\",\n\t\t\t\t\"creatorName\" : \"akshay.rao@daffodilsw.com\",\n\t\t\t\t\"creatorId\" : \"57711864bb9456b373f0956b\",\n\t\t\t\t\"_id\" : ObjectId(\"577e1b8d74e70df6284c1c9f\"),\n\t\t\t\t\"replyData\" : [ ]\n\t\t\t}\n\t\t]\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No post found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/comments.js",
    "groupTitle": "Comment"
  },
  {
    "type": "get",
    "url": "singlepost/:id",
    "title": "Get SinglePost",
    "name": "SinglePost",
    "group": "Comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Posts unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"577769c8bcdab8a5223c6395\"),\n\t\t\"createdOn\" : ISODate(\"2016-07-02T07:14:16Z\"),\n\t\t\"postedBy\" : \"akshay.rao@daffodilsw.com\",\n\t\t\"catType\" : \"quotes\",\n\t\t\"img\" : \"/images/Img-1661310859490186.jpeg\",\n\t\t\"title\" : \"hello\",\n\t\t\"user_id\" : \"57711864bb9456b373f0956b\",\n\t\t\"featured\" : true,\n\t\t\"flagBy\" : [\n\t\t\t\"57711864bb9456b373f0956b\"\n\t\t],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 0,\n\t\t\"likeBy\" : [ ],\n\t\t\"commentCount\" : 1,\n\t\t\"comments\" : [\n\t\t\t{\n\t\t\t\t\"commentedOn\" : \"577769c8bcdab8a5223c6395\",\n\t\t\t\t\"comment\" : \"Istanbul :)\",\n\t\t\t\t\"creatorName\" : \"akshay.rao@daffodilsw.com\",\n\t\t\t\t\"creatorId\" : \"57711864bb9456b373f0956b\",\n\t\t\t\t\"_id\" : ObjectId(\"577e1b8d74e70df6284c1c9f\"),\n\t\t\t\t\"replyData\" : [ ]\n\t\t\t}\n\t\t]\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No post found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/comments.js",
    "groupTitle": "Comment"
  },
  {
    "type": "post",
    "url": "submitcomment",
    "title": "Submit Comment",
    "name": "SubmitComment",
    "group": "Comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Posts unique id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>comment By the user .</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "creatorName",
            "description": "<p>Name of the user who commented.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "commentedOn",
            "description": "<p>id of post on which comment is done.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"577769c8bcdab8a5223c6395\"),\n\t\t\"createdOn\" : ISODate(\"2016-07-02T07:14:16Z\"),\n\t\t\"postedBy\" : \"akshay.rao@daffodilsw.com\",\n\t\t\"catType\" : \"quotes\",\n\t\t\"img\" : \"/images/Img-1661310859490186.jpeg\",\n\t\t\"title\" : \"hello\",\n\t\t\"user_id\" : \"57711864bb9456b373f0956b\",\n\t\t\"featured\" : true,\n\t\t\"flagBy\" : [\n\t\t\t\"57711864bb9456b373f0956b\"\n\t\t],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 0,\n\t\t\"likeBy\" : [ ],\n\t\t\"commentCount\" : 1,\n\t\t\"comments\" : [\n\t\t\t{\n\t\t\t\t\"commentedOn\" : \"577769c8bcdab8a5223c6395\",\n\t\t\t\t\"comment\" : \"Istanbul :)\",\n\t\t\t\t\"creatorName\" : \"akshay.rao@daffodilsw.com\",\n\t\t\t\t\"creatorId\" : \"57711864bb9456b373f0956b\",\n\t\t\t\t\"_id\" : ObjectId(\"577e1b8d74e70df6284c1c9f\"),\n\t\t\t\t\"replyData\" : [ ]\n\t\t\t}\n\t\t]\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No post found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/comments.js",
    "groupTitle": "Comment"
  },
  {
    "type": "get",
    "url": "submitReply",
    "title": "Submit Reply to Comment",
    "name": "SubmitReply",
    "group": "Comment",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Posts unique id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reply",
            "description": "<p>Reply By the user .</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "creatorName",
            "description": "<p>Name of the user who commented.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "creatorid",
            "description": "<p>unique id of user.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"577769c8bcdab8a5223c6395\"),\n\t\t\"createdOn\" : ISODate(\"2016-07-02T07:14:16Z\"),\n\t\t\"postedBy\" : \"akshay.rao@daffodilsw.com\",\n\t\t\"catType\" : \"quotes\",\n\t\t\"img\" : \"/images/Img-1661310859490186.jpeg\",\n\t\t\"title\" : \"hello\",\n\t\t\"user_id\" : \"57711864bb9456b373f0956b\",\n\t\t\"featured\" : true,\n\t\t\"flagBy\" : [\n\t\t\t\"57711864bb9456b373f0956b\"\n\t\t],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 0,\n\t\t\"likeBy\" : [ ],\n\t\t\"commentCount\" : 1,\n\t\t\"comments\" : [\n\t\t\t{\n\t\t\t\t\"replyData\" : [\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\" : ObjectId(\"5787310e7d1141b11b9f48c9\"),\n\t\t\t\t\t\t\"creatorId\" : \"57711864bb9456b373f0956b\",\n\t\t\t\t\t\t\"creatorName\" : \"akshay.rao@daffodilsw.com\",\n\t\t\t\t\t\t\"reply\" : \"love Istanbul <3\"\n\t\t\t\t\t}\n\t\t\t\t],\n\t\t\t\t\"_id\" : ObjectId(\"577e1b8d74e70df6284c1c9f\"),\n\t\t\t\t\"creatorId\" : \"57711864bb9456b373f0956b\",\n\t\t\t\t\"creatorName\" : \"akshay.rao@daffodilsw.com\",\n\t\t\t\t\"comment\" : \"Istanbul :)\",\n\t\t\t\t\"commentedOn\" : \"577769c8bcdab8a5223c6395\"\n\t\t\t}\n\t\t]\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No post found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/comments.js",
    "groupTitle": "Comment"
  },
  {
    "type": "get",
    "url": "dashboard/:categoryname",
    "title": "Get post according to category",
    "name": "CategoryPost",
    "group": "Dashboard",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryType",
            "description": "<p>Mandatory Type of category.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : false\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No post found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/dashboard.js",
    "groupTitle": "Dashboard"
  },
  {
    "type": "post",
    "url": "getPosts",
    "title": "Get filtered posts",
    "name": "FilteredPosts",
    "group": "Dashboard",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Filter",
            "description": "<p>filter like created on or most liked etc.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t[ { _id: 5787135babeb53a40d91e8cd,\n\t    user_id: '57711864bb9456b373f0956b',\n\t    title: 'new post',\n\t    img: '/uploads/post/london.jpg',\n\t    catType: 'london',\n\t    postedBy: 'akshay.rao@daffodilsw.com',\n\t    createdOn: Thu Jul 14 2016 09:51:47 GMT+0530 (IST),\n\t    __v: 0,\n\t    featured: false,\n\t    flagBy: [ '57711864bb9456b373f0956b' ],\n\t    likeBy: [],\n\t    comments: [] },\n\t  { _id: 577769c8bcdab8a5223c6395,\n\t    createdOn: Sat Jul 02 2016 12:44:16 GMT+0530 (IST),\n\t    postedBy: 'akshay.rao@daffodilsw.com',\n\t    catType: 'quotes',\n\t    img: '/images/Img-1661310859490186.jpeg',\n\t    title: 'hello',\n\t    user_id: '57711864bb9456b373f0956b',\n\t    __v: 0,\n\t    likeCount: 0,\n\t    commentCount: 1,\n\t    featured: true,\n\t    flagBy: [ '57711864bb9456b373f0956b' ],\n\t    likeBy: [],\n\t    comments: [ [Object] ] },\n\t  { _id: 571a0a924f59dc2a0e53ce88,\n\t    createdOn: Fri Apr 22 2016 16:57:14 GMT+0530 (IST),\n\t    postedBy: 'akshayrao571@gmail.com',\n\t    catType: 'others',\n\t    img: '/images/Img-1269243359565734.jpeg',\n\t    title: 'dgshjb',\n\t    user_id: '5711e8e3cba799dc2fc88f65',\n\t    __v: 0,\n\t    likeCount: 1,\n\t    featured: true,\n\t    flagBy: [],\n\t    likeBy: [ '571348b336196208545ec2d9' ],\n\t    comments: [] } ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/dashboard.js",
    "groupTitle": "Dashboard"
  },
  {
    "type": "get",
    "url": "dashboard",
    "title": "Get all posts",
    "group": "Dashboard",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : true\n\t}\n\t{\n\t\t\"_id\" : ObjectId(\"577769c8bcdab8a5223c6395\"),\n\t\t\"createdOn\" : ISODate(\"2016-07-02T07:14:16Z\"),\n\t\t\"postedBy\" : \"akshay.rao@daffodilsw.com\",\n\t\t\"catType\" : \"quotes\",\n\t\t\"img\" : \"/images/Img-1661310859490186.jpeg\",\n\t\t\"title\" : \"hello\",\n\t\t\"user_id\" : \"57711864bb9456b373f0956b\",\n\t\t\"featured\" : true,\n\t\t\"flagBy\" : [\n\t\t\t\"57711864bb9456b373f0956b\"\n\t\t],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 0,\n\t\t\"likeBy\" : [ ],\n\t\t\"commentCount\" : 1,\n\t\t\"comments\" : [\n\t\t\t{\n\t\t\t\t\"commentedOn\" : \"577769c8bcdab8a5223c6395\",\n\t\t\t\t\"comment\" : \"Istanbul :)\",\n\t\t\t\t\"creatorName\" : \"akshay.rao@daffodilsw.com\",\n\t\t\t\t\"creatorId\" : \"57711864bb9456b373f0956b\",\n\t\t\t\t\"_id\" : ObjectId(\"577e1b8d74e70df6284c1c9f\"),\n\t\t\t\t\"replyData\" : [ ]\n\t\t\t}\n\t\t]\n\t}\n\t{\n\t\t\"_id\" : ObjectId(\"5787135babeb53a40d91e8cd\"),\n\t\t\"user_id\" : \"57711864bb9456b373f0956b\",\n\t\t\"title\" : \"new post\",\n\t\t\"img\" : \"/uploads/post/london.jpg\",\n\t\t\"catType\" : \"london\",\n\t\t\"postedBy\" : \"akshay.rao@daffodilsw.com\",\n\t\t\"createdOn\" : ISODate(\"2016-07-14T04:21:47Z\"),\n\t\t\"featured\" : false,\n\t\t\"flagBy\" : [\n\t\t\t\"57711864bb9456b373f0956b\"\n\t\t],\n\t\t\"likeBy\" : [ ],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Error\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/dashboard.js",
    "groupTitle": "Dashboard",
    "name": "GetDashboard"
  },
  {
    "type": "get",
    "url": "featuredPosts/:categoryName",
    "title": "Featured posts by category",
    "name": "CategoryFeaturedPost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Posts Category.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Featured",
            "description": "<p>Post unfeature.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : true\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No Post found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "delete",
    "url": "deletepost/:id",
    "title": "Delete post",
    "name": "DeletePost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>posts unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t result: { ok: 1, n: 1 }\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Post not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "put",
    "url": "featurepost/:id",
    "title": "Feature a post",
    "name": "FeaturePost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Posts unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Featured",
            "description": "<p>Post feature.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : true\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Post not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "get",
    "url": "featuredPosts",
    "title": "Get featured Posts",
    "name": "FeaturedPosts",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Featured",
            "description": "<p>Post feature.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : true\n\t}\n\t{\n\t\t\"_id\" : ObjectId(\"577769c8bcdab8a5223c6395\"),\n\t\t\"createdOn\" : ISODate(\"2016-07-02T07:14:16Z\"),\n\t\t\"postedBy\" : \"akshay.rao@daffodilsw.com\",\n\t\t\"catType\" : \"quotes\",\n\t\t\"img\" : \"/images/Img-1661310859490186.jpeg\",\n\t\t\"title\" : \"hello\",\n\t\t\"user_id\" : \"57711864bb9456b373f0956b\",\n\t\t\"featured\" : true,\n\t\t\"flagBy\" : [\n\t\t\t\"57711864bb9456b373f0956b\"\n\t\t],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 0,\n\t\t\"likeBy\" : [ ],\n\t\t\"commentCount\" : 1,\n\t\t\"comments\" : [\n\t\t\t{\n\t\t\t\t\"commentedOn\" : \"577769c8bcdab8a5223c6395\",\n\t\t\t\t\"comment\" : \"Istanbul :)\",\n\t\t\t\t\"creatorName\" : \"akshay.rao@daffodilsw.com\",\n\t\t\t\t\"creatorId\" : \"57711864bb9456b373f0956b\",\n\t\t\t\t\"_id\" : ObjectId(\"577e1b8d74e70df6284c1c9f\"),\n\t\t\t\t\"replyData\" : [ ]\n\t\t\t}\n\t\t]\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No Post found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "put",
    "url": "post/flag/:postid",
    "title": "Flag a post",
    "name": "FlagPost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : false\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Post not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "post",
    "url": "flaggedposts",
    "title": "Flagged posts",
    "name": "FlaggedPost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Posts Category.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Featured",
            "description": "<p>Post unfeature.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"5787135babeb53a40d91e8cd\"),\n\t\t\"user_id\" : \"57711864bb9456b373f0956b\",\n\t\t\"title\" : \"new post\",\n\t\t\"img\" : \"/uploads/post/london.jpg\",\n\t\t\"catType\" : \"london\",\n\t\t\"postedBy\" : \"akshay.rao@daffodilsw.com\",\n\t\t\"createdOn\" : ISODate(\"2016-07-14T04:21:47Z\"),\n\t\t\"featured\" : false,\n\t\t\"flagBy\" : [\n\t\t\t\"57711864bb9456b373f0956b\"\n\t\t],\n\t\t\"likeBy\" : [ ],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"No Post found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "put",
    "url": "post/like/:postid",
    "title": "like a post",
    "name": "LikePost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : false\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Post not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "get",
    "url": "timeline",
    "title": "Timeline posts",
    "name": "TimelinePost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>posts unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : false\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Users post not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "put",
    "url": "unfeaturepost/:id",
    "title": "Unfeature a post",
    "name": "UnfeaturePost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Posts unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Featured",
            "description": "<p>Post unfeature.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : false\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Post not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "put",
    "url": "post/unflag/:postid",
    "title": "Unflag a post",
    "name": "UnflagPost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : false\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Post not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "put",
    "url": "post/unlike/:postid",
    "title": "Unlike a post",
    "name": "UnlikePost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : false\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Post not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "post",
    "url": "post",
    "title": "Upload a post",
    "name": "UploadPost",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Mandatory title of the Post.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>Mandatory Image.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "categoryType",
            "description": "<p>Mandatory Type of category.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n\t{\n\t\t\"_id\" : ObjectId(\"571a0a924f59dc2a0e53ce88\"),\n\t\t\"createdOn\" : ISODate(\"2016-04-22T11:27:14Z\"),\n\t\t\"postedBy\" : \"akshayrao571@gmail.com\",\n\t\t\"catType\" : \"others\",\n\t\t\"img\" : \"/images/Img-1269243359565734.jpeg\",\n\t\t\"title\" : \"dgshjb\",\n\t\t\"user_id\" : \"5711e8e3cba799dc2fc88f65\",\n\t\t\"flagBy\" : [ ],\n\t\t\"likeBy\" : [\n\t\t\t\"571348b336196208545ec2d9\"\n\t\t],\n\t\t\"comments\" : [ ],\n\t\t\"__v\" : 0,\n\t\t\"likeCount\" : 1,\n\t\t\"featured\" : false\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Could not upload this post\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/posts.js",
    "groupTitle": "Post"
  },
  {
    "type": "put",
    "url": "changepassword",
    "title": "Change password of user",
    "name": "ChangePassword",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "oldPassword",
            "description": "<p>old password of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "newPassword",
            "description": "<p>new password of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "confirmPassword",
            "description": "<p>confirm new password of the user.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n{\n    \"_id\" : ObjectId(\"57711864bb9456b373f0956b\"),\n    \"last_name\" : \"Rao\",\n    \"first_name\" : \"Akshay\",\n    \"password\" : \"$2a$08$Udpv54UGOGYESvuN/LucKurl7fj.N/lHRYn4ntqW1hMcfQW3x73CG\",\n    \"email\" : \"akshay.rao@daffodilsw.com\",\n    \"username\" : \"akshay.rao\",\n    \"__v\" : 0,\n    \"isAdmin\" : true,\n    \"description\" : \"Football = <3 \",\n    \"sex\" : \"Male\",\n   \"name\" : \"Akshay Rao\",\n    \"image\" : \"/uploads/profile/168590__anonymous-mask-style_p.jpg\",\n    \"verification_code\" : 23960,\n    \"approved\" : true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  message:\"Password not valid\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "editprofile",
    "title": "Edit User Profile",
    "name": "EditUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sex",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n{\n    \"_id\" : ObjectId(\"57711864bb9456b373f0956b\"),\n    \"last_name\" : \"Rao\",\n    \"first_name\" : \"Akshay\",\n    \"password\" : \"$2a$08$Udpv54UGOGYESvuN/LucKurl7fj.N/lHRYn4ntqW1hMcfQW3x73CG\",\n    \"email\" : \"akshay.rao@daffodilsw.com\",\n    \"username\" : \"akshay.rao\",\n    \"__v\" : 0,\n    \"isAdmin\" : true,\n    \"description\" : \"Football = <3 \",\n    \"sex\" : \"Male\",\n   \"name\" : \"Akshay Rao\",\n    \"image\" : \"/uploads/profile/168590__anonymous-mask-style_p.jpg\",\n    \"verification_code\" : 23960,\n    \"approved\" : true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  \"message\": \"No User Found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "forgotpassword/:email",
    "title": "Forgot Password by user",
    "name": "ForgotPassword",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory Email.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n { \n      message: 'Please check your email' \n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Emailexist",
            "description": "<p>The email couldn't be send.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Error sending email\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "getprofiledata",
    "title": "Get User Profile data",
    "name": "GetUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n{\n    \"_id\" : ObjectId(\"57711864bb9456b373f0956b\"),\n    \"last_name\" : \"Rao\",\n    \"first_name\" : \"Akshay\",\n    \"password\" : \"$2a$08$Udpv54UGOGYESvuN/LucKurl7fj.N/lHRYn4ntqW1hMcfQW3x73CG\",\n    \"email\" : \"akshay.rao@daffodilsw.com\",\n    \"username\" : \"akshay.rao\",\n    \"__v\" : 0,\n    \"isAdmin\" : true,\n    \"description\" : \"Football = <3 \",\n    \"sex\" : \"Male\",\n   \"name\" : \"Akshay Rao\",\n    \"image\" : \"/uploads/profile/168590__anonymous-mask-style_p.jpg\",\n    \"verification_code\" : 23960,\n    \"approved\" : true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  \"message\": \"No User Found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "login",
    "title": "Login User",
    "name": "LoginUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory Password.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n  \n  {\n    _id: 57711864bb9456b373f0956b,\n    last_name: 'Rao',\n    first_name: 'Akshay',\n    password: '$2a$08$WAben1FAOn37y61ydGt.0eMKACE6dwLsAZ/Bz9B.B93SEL8ZO.5yq',\n    email: 'akshay.rao@daffodilsw.com',\n    username: 'akshay.rao',\n    __v: 0,\n    isAdmin: false,\n    name: 'Akshay Rao',\n    verification_code: 60318,\n    approved:true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No",
            "description": "<p>User Found, Wrong Password, Email Verification Pending</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  \"message\": \"No User Found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "logout",
    "title": "Logout Current user",
    "name": "LogoutUser",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n\n{\n   message :'User logout successfully'\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  message :'User not login'\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "register",
    "title": "Register User",
    "name": "PostUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>Mandatory Firstname of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory Password.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n    \"_id\" : \"57711864bb9456b373f0956b\",\n    \"last_name\" : \"Rao\",\n    \"first_name\" : \"Akshay\",\n    \"password\" : \"$2a$08$Udpv54UGOGYESvuN/LucKurl7fj.N/lHRYn4ntqW1hMcfQW3x73CG\",\n    \"email\" : \"akshay.rao@daffodilsw.com\",\n    \"username\" : \"akshay.rao\",\n    \"__v\" : 0,\n    \"isAdmin\" : false,\n    \"name\" : \"Akshay Rao\",\n    \"verification_code\" : 23960,\n    \"approved\" : false\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Emailexist",
            "description": "<p>The email of the User already exist.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Email already exist\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "resetPassword/:token/:id",
    "title": "Reset Password",
    "name": "ResetPassword",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n  {\n    _id: 57711864bb9456b373f0956b,\n    last_name: 'Rao',\n    first_name: 'Akshay',\n    password: '$2a$08$WAben1FAOn37y61ydGt.0eMKACE6dwLsAZ/Bz9B.B93SEL8ZO.5yq',\n    email: 'akshay.rao@daffodilsw.com',\n    username: 'akshay.rao',\n    __v: 0,\n    isAdmin: false,\n    name: 'Akshay Rao',\n    verification_code: 60318,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"The passwords didn't matched\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "user/verify/:token/:id",
    "title": "Verify new user",
    "name": "VerifyUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "token",
            "description": "<p>Users verification code.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n  {\n    _id: 57711864bb9456b373f0956b,\n    last_name: 'Rao',\n    first_name: 'Akshay',\n    password: '$2a$08$WAben1FAOn37y61ydGt.0eMKACE6dwLsAZ/Bz9B.B93SEL8ZO.5yq',\n    email: 'akshay.rao@daffodilsw.com',\n    username: 'akshay.rao',\n    __v: 0,\n    isAdmin: false,\n    name: 'Akshay Rao',\n    verification_code: 60318,\n    approved:true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"The passwords didn't matched\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  }
] });
