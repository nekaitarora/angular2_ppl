import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';
import { Service } from './service';
import { Headers, Http } from '@angular/http';
import { RouteParams } from '@angular/router-deprecated';
import {Modal, BS_MODAL_PROVIDERS} from 'angular2-modal/plugins/bootstrap';

@Component({
	selector: 'singlePost',
	templateUrl: "../assets/singlepost.html"
})	

export class singlePostComponent implements OnInit {
public postData;
public error;
public elements;
public comment;
public commentdata;
public user;
public User;
public postdata;
public replydiv = -1;
public replydata;
constructor(private router: Router,private service:Service,private routeParams: RouteParams,public modal: Modal, viewContainer: ViewContainerRef){
	this.postData =[];
	this.postdata = {};
	this.comment = {};
	this.commentdata = {};
	this.elements =[];
  // this.user = [];
}	

ngOnInit() {
  // if(localStorage.getItem('user') == null){
  //   this.router.navigate(['Login']);    
  // }
  this.postData.likeBy= [];
  this.postData.flagBy= [];
  this.getUser();
  this.getDetails();
  this.getComments();
}  

getUser() {
  if(localStorage.getItem('user') !== null){
  this.service.list = true;
  this.User = localStorage.getItem('user');
  console.log('user here is',this.User);
  this.user = JSON.parse(this.User);	
}
}

getDetails() {
    	let id = this.routeParams.get('id');
    	console.log('id is as ',id);
    	let url = `${'singlepost'}/${id}`;
    	this.service.get(url)
      	 .then((res) => {
      	 	this.postData = res;
      	 	console.log('data of singlepost is',this.postData);
      	 })
      	 .catch(error => this.error = error);	
}

getComments() {
    	let id = this.routeParams.get('id');
    	console.log('id is as ',id);
    	let url = `${'getComment'}/${id}`;
    	this.service.get(url)
      	 .then((res) => {
      	 	this.elements = res;
      	 	// console.log('comment of this post is',this.elements);
      	 })
      	 .catch(error => this.error = error);	
}

submitcomment() {
  if(this.user){
	let id = this.routeParams.get('id');
	this.commentdata.comment = this.comment.comment;
	this.commentdata.commentedOn = id;
	this.commentdata.creatorId = this.user._id;
	this.commentdata.creatorName = this.user.email;
	this.service.post('submitcomment',this.commentdata)
	.then((res) => {
		this.getComments();
    // console.log('the response of comment is',res);
		this.comment = {};
	})
	.catch(error => this.error = error);
}
else{
  // alert('log in to submit comment');
        return this.modal.alert()
                .size('md')
                .isBlocking(true)
                .showClose(true)
                .title('You must be logged in to submit comment')
                .open()
                .catch(err => alert("ERROR"))
                .then(dialog => dialog.result)
                .then(result => { 
                  });   
}
}

flag(postData) {
  let url = `${'post/flag'}/${postData._id}`;
  this.putCall(url);
}

unflag(postData) {
  let url = `${'post/unflag'}/${postData._id}`;
  this.putCall(url);
}

like(postData) {
  let url = `${'post/like'}/${postData._id}`;
  this.putCall(url);
}

unlike(postData) {
  let url = `${'post/unlike'}/${postData._id}`;
  this.putCall(url);
}

putCall(url) {
  this.postdata.postid = this.postData._id;  
  this.postdata.user_id = this.user._id;
  this.service.put(url,this.postdata)
  .then((res) => {
    console.log('response here is',res);
    let response = [];
    response.push(res);
    this.postData = response;
  })
  .catch(error => this.error = error)   
}

replyBox(index) {
  if(this.user){
  this.replydiv = index;
}
else{
  // alert('Log in to reply to this comment');
        return this.modal.alert()
                .size('md')
                .isBlocking(true)
                .showClose(true)
                .title('You must be logged in to reply to this comment')
                .open()
                .catch(err => alert("ERROR"))
                .then(dialog => dialog.result)
                .then(result => { 
                  });   
}
}

reply(post,reply,index) {
  if(this.user) {
  console.log('the data here is',post,reply,index);
  let replydata = {'postid':post.commentedOn,'reply':reply,'index':index,'creatorName':this.user.email,'creatorId':this.user._id};
  this.service.post('submitReply',replydata)
  .then((res) => {
    console.log('the reply here is',res);
    this.getDetails();
    this.getComments();
    this.replydata = "";
  })
  .catch(error => this.error = error);
}
}

unknownFlag() {
  // alert('You must be logged in to flag this post');
        return this.modal.alert()
                .size('md')
                .isBlocking(true)
                .showClose(true)
                .title('You must be logged in to flag this post')
                .open()
                .catch(err => alert("ERROR"))
                .then(dialog => dialog.result)
                .then(result => { 
                  });  
}

unknownLike() {
  // alert('You must be logged in to like this post');
        return this.modal.alert()
                .size('md')
                .isBlocking(true)
                .showClose(true)
                .title('You must be logged in to like this post')
                .open()
                .catch(err => alert("ERROR"))
                .then(dialog => dialog.result)
                .then(result => { 
                  });   
}



}	
