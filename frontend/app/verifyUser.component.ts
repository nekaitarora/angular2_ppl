import { Component,OnInit } from '@angular/core';
import { Service } from './service';
import { RouteParams } from '@angular/router-deprecated';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';

@Component({
	selector: 'verifyUser',
	template : "<p>hello</p>",
  directives: [ROUTER_DIRECTIVES]  
})	

export class verifyUserComponent implements OnInit{
public res;
public error;

constructor(private service:Service,private routeParams: RouteParams,private router: Router){
}	

ngOnInit() {
  // this.getUserDetails();
  this.getUser();
}

getUser() {
  let id = this.routeParams.get('id');
  let token = this.routeParams.get('token');
  let url = `${'user/verify'}/${token}/${id}`;
  console.log('the email and token of user is',id,token);
  this.service.put(url,{})
  .then((res) => {
  	this.res = res;
  	console.log('response of user is',this.res);
    this.router.navigate(['Login']);        
  })
  .catch(error => this.error = error);

}

}