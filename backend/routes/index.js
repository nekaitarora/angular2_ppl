

module.exports = function(app) {
	app.use('/',require('./users'));

	app.use('/',require('./categories'));

	app.use('/',require('./posts'));

	app.use('/',require('./dashboard'));

	app.use('/',require('./comments'));	
}
