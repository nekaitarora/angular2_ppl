import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Data } from './login.component';

@Injectable()

export class Service {
public url = 'http://localhost:4000';
public data = [];
public list = false;
constructor(private http: Http) { }

  get(extraUrl): Promise<Data[]> {
  	let path = `${this.url}/${extraUrl}`;
    return this.http.get(path,{withCredentials: true})
               .toPromise()
               .then(response => response.json())
               .catch(this.handleError);
  }

  post(extraUrl,data): Promise<Data[]> {
  	let path = `${this.url}/${extraUrl}`;
    console.log('data in post call is',data);
  	return this.http.post(path,data,{withCredentials: true})
  	.toPromise()
  	.then((response) => response.json())
  	.catch(this.handleError);
  }

  put(extraUrl,data) {
  	let path = `${this.url}/${extraUrl}`;
  	return this.http.put(path,data,{withCredentials: true})
    .toPromise()
    .then((response) => response.json())
    .catch(this.handleError);
  }

  delete(extraUrl) {
  	let path = `${this.url}/${extraUrl}`;
  	return this.http.delete(path,{withCredentials: true})
    .toPromise()
    .then((response) => response.json())
    .catch(this.handleError);
  }

  private handleError(error: any) {
    // console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
