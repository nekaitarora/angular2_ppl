import {Component , OnInit} from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { FormBuilder, Validators } from '@angular/common';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';
import { NgForm }    from '@angular/common';
import { Service } from './service';
import { AppComponent } from './app.component';
import {CookieService} from 'angular2-cookie/core';

export class Data {
  _id: String;
}

@Component({
	selector: 'Login',
	templateUrl: './assets/login.html',
  directives: [ROUTER_DIRECTIVES],  
  providers: [CookieService]
})

export class loginComponent implements OnInit{
// public response; 
public error;
public localData;
public data;
constructor(private http: Http,
  private router: Router,
  private service: Service,
  private _cookieService:CookieService) {
	this.data = {};
}

ngOnInit() {
  console.log('this is oninit of login');
  if(localStorage.getItem('user') !== null){
    this.router.navigate(['Dashboard']);    
  }
  this.setCookie();
}

  setCookie(){
    return this._cookieService.put('akshay','rao');
  }

// checkservice() {
//   this.service.get('akshay')
//   .then((response) => {
//     this.response = response;
//     console.log('hello the response from service is',this.response);
//   })
//   .catch(error => this.error = error);
// }

loginUser() {
  this.service.post('login',this.data)
  .then((res) => {
    // this.res = res;
    console.log('res is as',res);
    if(res) {
        this.localData = JSON.stringify(res);
        localStorage.setItem('user', this.localData);
        this.service.list = true;
        this.router.navigate(['Dashboard']);      
    }
  })
  .catch(this.handleError);
}

// loginUser(): Promise<Data> {
//     let headers = new Headers({
//       'Content-Type': 'application/json'});
//       return this.http.post('http://localhost:4040/login',JSON.stringify(this.data), {headers: headers})
//       .toPromise()
//       .then((res) => {
//         console.log('the response here is',res.json());
//       if(res.json()){        
//         this.localData = JSON.stringify(res.json());
//         localStorage.setItem('user', this.localData);
//         this.router.navigate(['Dashboard']);
//         console.log('the user in local storage is',localStorage.getItem('user'));        
//         return res.json();
//       }
//       })
//       .catch(this.handleError);
// }

  private handleError(error: any) {
    alert('username and password does not match');
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}