var express = require('express'),
	router = express.Router(),
	posts_db = require('../libs/posts/posts_db'),
	posts_api = require('../libs/posts/posts_api'),	
	mail = require('../config/mail')
	passport = require('passport'),
	fs = require('fs');


/**
 * @api {get} dashboard Get all posts
 * @apiGroup Dashboard
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : true
 *	}
 *	{
 *		"_id" : ObjectId("577769c8bcdab8a5223c6395"),
 *		"createdOn" : ISODate("2016-07-02T07:14:16Z"),
 *		"postedBy" : "akshay.rao@daffodilsw.com",
 *		"catType" : "quotes",
 *		"img" : "/images/Img-1661310859490186.jpeg",
 *		"title" : "hello",
 *		"user_id" : "57711864bb9456b373f0956b",
 *		"featured" : true,
 *		"flagBy" : [
 *			"57711864bb9456b373f0956b"
 *		],
 *		"__v" : 0,
 *		"likeCount" : 0,
 *		"likeBy" : [ ],
 *		"commentCount" : 1,
 *		"comments" : [
 *			{
 *				"commentedOn" : "577769c8bcdab8a5223c6395",
 *				"comment" : "Istanbul :)",
 *				"creatorName" : "akshay.rao@daffodilsw.com",
 *				"creatorId" : "57711864bb9456b373f0956b",
 *				"_id" : ObjectId("577e1b8d74e70df6284c1c9f"),
 *				"replyData" : [ ]
 *			}
 *		]
 *	}
 *	{
 *		"_id" : ObjectId("5787135babeb53a40d91e8cd"),
 *		"user_id" : "57711864bb9456b373f0956b",
 *		"title" : "new post",
 *		"img" : "/uploads/post/london.jpg",
 *		"catType" : "london",
 *		"postedBy" : "akshay.rao@daffodilsw.com",
 *		"createdOn" : ISODate("2016-07-14T04:21:47Z"),
 *		"featured" : false,
 *		"flagBy" : [
 *			"57711864bb9456b373f0956b"
 *		],
 *		"likeBy" : [ ],
 *		"comments" : [ ],
 *		"__v" : 0
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Error"
 *     }
 */





router.get('/dashboard',function(req,res){
	console.log('user in request is ',req.user);
	var post = {};
	var data = {};
	posts_api.findPost(post,data,function(err,result){
		if(err){res.send(err);}
		res.json(result);
	})	
})


/**
 * @api {get} dashboard/:categoryname Get post according to category
 * @apiName CategoryPost
 * @apiGroup Dashboard
 *
 * @apiParam {String} categoryType     Mandatory Type of category.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : false
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No post found"
 *     }
 */





router.get('/dashboard/:categoryname',function(req,res){
	console.log('url for this is',req.params.categoryname);
	var post = {};
	var data ={};
	post.catType = req.params.categoryname;
	posts_api.findPost(post,data,function(err,result){
		if(err){res.send(err);}
		res.json(result);
	})	
})


/**
 * @api {post} getPosts Get filtered posts
 * @apiName FilteredPosts
 * @apiGroup Dashboard
 *
 * @apiParam {String} Filter     filter like created on or most liked etc.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	[ { _id: 5787135babeb53a40d91e8cd,
 *	    user_id: '57711864bb9456b373f0956b',
 *	    title: 'new post',
 *	    img: '/uploads/post/london.jpg',
 *	    catType: 'london',
 *	    postedBy: 'akshay.rao@daffodilsw.com',
 *	    createdOn: Thu Jul 14 2016 09:51:47 GMT+0530 (IST),
 *	    __v: 0,
 *	    featured: false,
 *	    flagBy: [ '57711864bb9456b373f0956b' ],
 *	    likeBy: [],
 *	    comments: [] },
 *	  { _id: 577769c8bcdab8a5223c6395,
 *	    createdOn: Sat Jul 02 2016 12:44:16 GMT+0530 (IST),
 *	    postedBy: 'akshay.rao@daffodilsw.com',
 *	    catType: 'quotes',
 *	    img: '/images/Img-1661310859490186.jpeg',
 *	    title: 'hello',
 *	    user_id: '57711864bb9456b373f0956b',
 *	    __v: 0,
 *	    likeCount: 0,
 *	    commentCount: 1,
 *	    featured: true,
 *	    flagBy: [ '57711864bb9456b373f0956b' ],
 *	    likeBy: [],
 *	    comments: [ [Object] ] },
 *	  { _id: 571a0a924f59dc2a0e53ce88,
 *	    createdOn: Fri Apr 22 2016 16:57:14 GMT+0530 (IST),
 *	    postedBy: 'akshayrao571@gmail.com',
 *	    catType: 'others',
 *	    img: '/images/Img-1269243359565734.jpeg',
 *	    title: 'dgshjb',
 *	    user_id: '5711e8e3cba799dc2fc88f65',
 *	    __v: 0,
 *	    likeCount: 1,
 *	    featured: true,
 *	    flagBy: [],
 *	    likeBy: [ '571348b336196208545ec2d9' ],
 *	    comments: [] } ]
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Error"
 *     }
 */





router.post('/getPosts',function(req,res){
	console.log('req body here is',req.body);
	var post = {};
	var data= {};
	data = req.body;
	posts_api.findPost(post,data,function(err,result){
		if(err){res.send(err);}
		console.log('result is',result);
		res.json(result);
	})	
})

module.exports =router;