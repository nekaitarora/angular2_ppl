

var express = require('express'),
	router = express.Router(),
	users_db = require('../libs/users/users_db'),
	users_api = require('../libs/users/users_api'),
	mail = require('../config/mail')
	passport = require('passport'),
	fs = require('fs');

var hash = new users_db();

/**
 * @api {post} register Register User
 * @apiName PostUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 * @apiParam {String} first_name Mandatory Firstname of the User.
 * @apiParam {String} email     Mandatory Email.
 * @apiParam {String} password     Mandatory Password.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *        "_id" : "57711864bb9456b373f0956b",
 *        "last_name" : "Rao",
 *        "first_name" : "Akshay",
 *        "password" : "$2a$08$Udpv54UGOGYESvuN/LucKurl7fj.N/lHRYn4ntqW1hMcfQW3x73CG",
 *        "email" : "akshay.rao@daffodilsw.com",
 *        "username" : "akshay.rao",
 *        "__v" : 0,
 *        "isAdmin" : false,
 *        "name" : "Akshay Rao",
 *        "verification_code" : 23960,
 *        "approved" : false
 *    }
 *
 * @apiError Emailexist The email of the User already exist.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Email already exist"
 *     }
 */


router.post('/register',function(req, res) {
    // console.log('the req in register is',req.body);
        // var user = new users_db(); 
        var user = {};
        user.username = req.body.username;
        user.email = req.body.email;
        user.password = hash.generateHash(req.body.password);
        user.verification_code = Math.floor(Math.random() * 90000) + 10000;
        user.first_name = req.body.first_name;
        user.last_name = req.body.last_name;
        users_api.createUser(user,function(err,result){
            if(err) { res.send(err);}
            mail.welcomemail(user.verification_code,user.email,result._id);       
            res.json({ message: 'user created!' });            
        }) 
});


/**
 * @api {post} forgotpassword/:email Forgot Password by user
 * @apiName ForgotPassword
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 * @apiParam {String} email     Mandatory Email.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      { 
 *           message: 'Please check your email' 
 *      }
 *
 * @apiError Emailexist The email couldn't be send.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Error sending email"
 *     }
 */






router.post('/forgotpassword/:email',function(req,res){
    var userEmail = req.params.email;
    // console.log('the user here is',req.params.email);
    var verification_code = Math.floor(Math.random() * 90000) + 10000; 
    var user = {};
    user.email = userEmail;
    var updatedata = {};
    updatedata.verification_code = verification_code; 
        users_api.updateUser(user,updatedata,function(err,result){
            if(err) { res.send(err);}
            mail.resetPasswordMail(verification_code,userEmail,result._id);
            res.json({ message: 'Please check your email' });            
        })
})

/**
 * @api {put} resetPassword/:token/:id Reset Password
 * @apiName ResetPassword
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *      _id: 57711864bb9456b373f0956b,
 *      last_name: 'Rao',
 *      first_name: 'Akshay',
 *      password: '$2a$08$WAben1FAOn37y61ydGt.0eMKACE6dwLsAZ/Bz9B.B93SEL8ZO.5yq',
 *      email: 'akshay.rao@daffodilsw.com',
 *      username: 'akshay.rao',
 *      __v: 0,
 *      isAdmin: false,
 *      name: 'Akshay Rao',
 *      verification_code: 60318,
 *  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "The passwords didn't matched"
 *     }
 */

router.put('/resetPassword/:token/:id',function(req,res){
    var user = {};
    user._id = req.params.id;
    users_api.findUser(user,function(error,result){
        if(error) { res.send(error); }
        var newPassword = hash.generateHash(req.body.newPassword);
        var verification_code = Math.floor(Math.random() * 90000) + 10000;
        var userdata = {};
        userdata = result[0];
        userdata.password = newPassword;
        userdata.verification_code = verification_code;
        users_api.updateUser(user,userdata,function(err,data){
            if(err) { res.send(err);}
            res.json(data);
        })
    })
})


// router.get('/verifyuser/email/:_id',function(req,res){
//     db.user.findById(req.params._id,function(err,user){
//         if(err)
//             res.send(err);
//         res.json(user);
//     })
// });

router.get('/user/resetPassword/:token/:id',function(req,res){
    var user = {};
    user._id = req.params.id;
    user.verification_code = req.params.token;
    users_api.findUser(user,function(err,result){
        if(err) { res.send(err); }
        res.json(result);
    })
})

/**
 * @api {put} user/verify/:token/:id Verify new user
 * @apiName VerifyUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 * @apiParam {Number} token Users verification code.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *      _id: 57711864bb9456b373f0956b,
 *      last_name: 'Rao',
 *      first_name: 'Akshay',
 *      password: '$2a$08$WAben1FAOn37y61ydGt.0eMKACE6dwLsAZ/Bz9B.B93SEL8ZO.5yq',
 *      email: 'akshay.rao@daffodilsw.com',
 *      username: 'akshay.rao',
 *      __v: 0,
 *      isAdmin: false,
 *      name: 'Akshay Rao',
 *      verification_code: 60318,
 *      approved:true
 *  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "The passwords didn't matched"
 *     }
 */


router.put('/user/verify/:token/:id',function(req,res){
    // console.log('user ',req.params.id,req.params.token);
    var user = {};
    user._id = req.params.id;
    user.verification_code = req.params.token;
    var data = {};
    data.approved = true;
    users_api.updateUser(user,data,function(err,result){
        if(err) { res.send(err); }
        res.json(result);
    })
});

/**
* @api {post} login Login User
* @apiName LoginUser
* @apiGroup User
*
* @apiParam {String} email     Mandatory email.
* @apiParam {String} password     Mandatory Password.
*
*
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*    
 *    {
 *      _id: 57711864bb9456b373f0956b,
 *      last_name: 'Rao',
 *      first_name: 'Akshay',
 *      password: '$2a$08$WAben1FAOn37y61ydGt.0eMKACE6dwLsAZ/Bz9B.B93SEL8ZO.5yq',
 *      email: 'akshay.rao@daffodilsw.com',
 *      username: 'akshay.rao',
 *      __v: 0,
 *      isAdmin: false,
 *      name: 'Akshay Rao',
 *      verification_code: 60318,
 *      approved:true
 *  }

* @apiError No User Found, Wrong Password, Email Verification Pending
*
* @apiErrorExample {json} Error-Response:
*     HTTP/1.1 400 Not Found
*     {
*       "message": "No User Found"
*     }
*
*/


router.post('/login',function(req,res,next){
    passport.authenticate('local-login',function(err,user,info){
        if(user && user._id){
        req.logIn(user, function(err) {
            // console.log('im gsvah',user);
            if (err) { return next(err); }
            return;
        });
    }
        if(err)   return res.status(400).send({message : err}); 
        if(info)  return res.status(400).send({message : info});
        if(user)  return res.status(200).send(user);        
    })(req, res, next);
});

/**
* @api {post} editprofile Edit User Profile
* @apiName EditUser
* @apiGroup User
*
* @apiParam {String} name 
* @apiParam {String} sex     
* @apiParam {String} description     
* @apiParam {String} image     
*
*
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*    
*    {
*        "_id" : ObjectId("57711864bb9456b373f0956b"),
*        "last_name" : "Rao",
*        "first_name" : "Akshay",
*        "password" : "$2a$08$Udpv54UGOGYESvuN/LucKurl7fj.N/lHRYn4ntqW1hMcfQW3x73CG",
*        "email" : "akshay.rao@daffodilsw.com",
*        "username" : "akshay.rao",
*        "__v" : 0,
*        "isAdmin" : true,
*        "description" : "Football = <3 ",
*        "sex" : "Male",
*       "name" : "Akshay Rao",
*        "image" : "/uploads/profile/168590__anonymous-mask-style_p.jpg",
*        "verification_code" : 23960,
*        "approved" : true
*    }
*
* @apiErrorExample {json} Error-Response:
*     HTTP/1.1 400 Not Found
*     {
*       "message": "No User Found"
*     }
*
*/



router.put("/editprofile",function(req,res){
            var user = {};
            user._id = req.body._id;
            var data = {};
            req.body.first_name ? data.first_name = req.body.first_name : null;
            req.body.last_name ? data.last_name = req.body.last_name : null;
            req.body.name ? data.name = req.body.name : null;
            req.body.sex ? data.sex = req.body.sex : null;
            req.body.description ? data.description = req.body.description : null;
            req.body.image ? data.image = req.body.image : null;
            users_api.updateUser(user,data,function(err,result){
                if(err) {res.send(err);}
                res.json(result);
            })
})

/**
* @api {post} getprofiledata Get User Profile data
* @apiName GetUser
* @apiGroup User
*
* @apiParam {Number} id Users unique ID.
*
*
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*    
*    {
*        "_id" : ObjectId("57711864bb9456b373f0956b"),
*        "last_name" : "Rao",
*        "first_name" : "Akshay",
*        "password" : "$2a$08$Udpv54UGOGYESvuN/LucKurl7fj.N/lHRYn4ntqW1hMcfQW3x73CG",
*        "email" : "akshay.rao@daffodilsw.com",
*        "username" : "akshay.rao",
*        "__v" : 0,
*        "isAdmin" : true,
*        "description" : "Football = <3 ",
*        "sex" : "Male",
*       "name" : "Akshay Rao",
*        "image" : "/uploads/profile/168590__anonymous-mask-style_p.jpg",
*        "verification_code" : 23960,
*        "approved" : true
*    }
*
* @apiErrorExample {json} Error-Response:
*     HTTP/1.1 400 Not Found
*     {
*       "message": "No User Found"
*     }
*
*/




router.post('/getprofiledata',function(req,res){
    // console.log("the user is1221 ",req.body);
    var user = {};
    user._id = req.body.id;
    users_api.findUser(user,function(err,result){
        if(err) {res.send(err);}
        res.json(result);
    })
})

/**
* @api {put} changepassword Change password of user
* @apiName ChangePassword
* @apiGroup User
*
* @apiParam {Number} id Users unique ID.
* @apiParam {String} oldPassword old password of the user.
* @apiParam {String} newPassword new password of the user.
* @apiParam {String} confirmPassword confirm new password of the user.
*
*
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*    
*    {
*        "_id" : ObjectId("57711864bb9456b373f0956b"),
*        "last_name" : "Rao",
*        "first_name" : "Akshay",
*        "password" : "$2a$08$Udpv54UGOGYESvuN/LucKurl7fj.N/lHRYn4ntqW1hMcfQW3x73CG",
*        "email" : "akshay.rao@daffodilsw.com",
*        "username" : "akshay.rao",
*        "__v" : 0,
*        "isAdmin" : true,
*        "description" : "Football = <3 ",
*        "sex" : "Male",
*       "name" : "Akshay Rao",
*        "image" : "/uploads/profile/168590__anonymous-mask-style_p.jpg",
*        "verification_code" : 23960,
*        "approved" : true
*    }
*
* @apiErrorExample {json} Error-Response:
*     HTTP/1.1 400 Not Found
*     {
*       message:"Password not valid"
*     }
*
*/




router.put('/changepassword',function(req,res){
    if(req.user.validPassword(req.body.oldPassword)){
        var user={};
        user._id = req.body.id;
        users_api.findUser(user,function(err,result){
            if(err) {res.send(err);}
            result.password = hash.generateHash(req.body.newPassword);
            users_api.updateUser(user,result,function(err,data){
                if(err) {res.send(err);}
                res.json(data);
            })
        })
    }else{
        return res.json({message:"Password not valid"});
    }
})

/**
* @api {get} logout Logout Current user
* @apiName LogoutUser
* @apiGroup User
*
*
*
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*    
*    {
*       message :'User logout successfully'
*    }
*
* @apiErrorExample {json} Error-Response:
*     HTTP/1.1 400 Not Found
*     {
*       message :'User not login'
*     }
*
*/




router.get('/logout', function(req, res) {
    if(req.user){
        req.logout();
        return res.status(200).send({message :'User logout successfully'})  
    }else{
        return res.status(400).send({message :'User not login'});   
    }
});

module.exports =router;