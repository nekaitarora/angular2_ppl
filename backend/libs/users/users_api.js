


var users_db = require('./users_db');

module.exports = {

    createUser: function(user,cb) {
    	var newUser = new users_db(user);
    	newUser.save(function(err,result){
    		if(err) { return cb(err,null); }
    		return cb(null,result);
    	})
    },

    findUser: function(user,cb) {
    	users_db.find(user).exec(function(err,result){
    		if(err) { return cb(err,null); }
    		return cb(null,result);
    	})
    },

    updateUser: function(user,data,cb) {
    	users_db.findOneAndUpdate(user,{$set:data},{new:true,upsert:true}).exec(function(err,result){
    		if(err) { return cb(err,null); }
    		return cb(null,result);
    	})
    }

}    	