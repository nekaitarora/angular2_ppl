
// load the things we need
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt   = require('bcrypt-nodejs'),
    uniqueValidator = require('mongoose-unique-validator');
    
var userSchema = new Schema ({
	username:String,
	email:String,
	image: String,
	password:String,
	first_name:String,
	last_name:String,
	verification_code:Number,
	verified:Boolean,
	reset_pass_token:String,
	role:String,
	name:String,
	description:String,
	sex:String,
	isAdmin:Boolean,
	approved : { type: Boolean, default: false }	
},{collection:"user"});


// Apply the uniqueValidator plugin to userSchema. 
userSchema.plugin(uniqueValidator);


userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

 module.exports = mongoose.model("user",userSchema);