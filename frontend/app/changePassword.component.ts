import { Component, OnInit } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';
import { Service } from './service';

@Component({
	selector: 'changePassword',
	templateUrl: "../assets/changePassword.html"
  	// directives: [ROUTER_DIRECTIVES]	

})	

export class changePasswordComponent implements OnInit {
public data;
public user;
public User;
public error;

constructor(private router: Router,private service: Service) {
	this.data = {};
}

ngOnInit() {
	if(localStorage.getItem('user') == null){
		this.router.navigate(['Login']);
	}
  	this.service.list = true;	
	this.getUser();
}

getUser() {
  this.User = localStorage.getItem('user');
  console.log('user here is',this.User);
  this.user = JSON.parse(this.User);	
}

changePassword() {
	this.data.id = this.user._id;
	if(this.data.newPassword == this.data.confirmPassword){
		this.service.put('changepassword',this.data)
		.then((res) => {
			alert('password changed successfully');
			this.router.navigate(['Dashboard'])
		})
		.catch(error => this.error = error);
	}
	else {
		alert("the passwords didn't matched");
	}

}

}