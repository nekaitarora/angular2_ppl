

var posts_db = require('./posts_db');

module.exports = {

    createPost: function(post,cb) {
    	var newPost = new posts_db(post);
    	newPost.save(function(err,result){
    		if(err) { return cb(err,null); }
    		return cb(null,result);
    	})
    },

    likePost: function(post,id,cb) {
    	posts_db.findOneAndUpdate({"_id":post},{$push:{'likeBy':id},$inc:{likeCount:1}},{$upsert:true,"new":true}).exec(function(err,result){
    		if(err) { return cb(err,null);}
            console.log('result here is',result);
    		return cb(null,result);
    	})
    },

    unlikePost: function(post,id,cb) {
    	posts_db.findOneAndUpdate({"_id":post},{$pull:{'likeBy':id},$inc:{likeCount:-1}},{$upsert:true,"new":true}).exec(function(err,result){
    		if(err) { return cb(err,null);}
    		return cb(null,result);
    	})
    },    

    flagPost: function(post,id,cb) {
    	posts_db.findOneAndUpdate({"_id":post},{$push:{'flagBy':id}},{$upsert:true,"new":true}).exec(function(err,result){
    		if(err) { return cb(err,null);}
    		return cb(null,result);
    	})
    },

    unflagPost: function(post,id,cb) {
    	posts_db.findOneAndUpdate({"_id":post},{$pull:{'flagBy':id}},{$upsert:true,"new":true}).exec(function(err,result){
    		if(err) { return cb(err,null);}
    		return cb(null,result);
    	})
    },

    findPost: function(id,data,cb) {
        console.log('the id is',id);
        if(data) {
            posts_db.find(id).sort(data).exec(function(err,result){
                if(err) {return cb(err,null);}
                return cb(null,result);
            })        
        }
        else {
    	    posts_db.find(id).exec(function(err,result){
    		  if(err) {return cb(err,null);}
    		  return cb(null,result);
    	   })
        }
    },

    removePost: function(id,cb) {
    	posts_db.remove(id).exec(function(err,result){
    		if(err) {return cb(err,null);}
    		return cb(null,result);
    	})
    },

    updatePost: function(id,post,cb) {
        console.log('the data is',id,post);
    	posts_db.findOneAndUpdate(id,{$set:post},{$upsert:true,"new":true}).exec(function(err,result){
    		if(err) { return cb(err,null);}
    		return cb(null,result);
    	})
    },

    updateComments: function(id,data,cb) {
    	posts_db.findOneAndUpdate(id,{$push:{comments:data},$inc:{commentCount:1}},{$upsert:true,"new":true}).exec(function(err,result){
    		if(err) {return cb(err,null);}
    		return cb(null,result);
    	})
    },

    updateReply: function(post,comment,cb) {
        console.log('the data is',post,comment);
        posts_db.update({'_id':post._id , "comments._id" : comment._id},{$set : {"comments.$" : comment }},function(err,result){
            if(err) { return cb(err,null);}
            console.log("result",result);
            return cb(null,result);
        })
    },    

}	