var express = require('express'),
	router = express.Router(),
	posts_db = require('../libs/posts/posts_db'),
	posts_api = require('../libs/posts/posts_api'),
	mail = require('../config/mail')
	passport = require('passport'),
	fs = require('fs');


/**
 * @api {post} post Upload a post
 * @apiName UploadPost
 * @apiGroup Post
 *
 *
 * @apiParam {Number} id Users unique ID.
 * @apiParam {String} title Mandatory title of the Post.
 * @apiParam {String} image     Mandatory Image.
 * @apiParam {String} categoryType     Mandatory Type of category.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : false
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Could not upload this post"
 *     }
 */



router.post("/post",function(req,res){
			console.log('post data here is',req.body);
            var post = {};
            post.user_id = req.body.id;
            post.title = req.body.title;
            post.img = req.body.path;
            post.catType = req.body.categories;
            post.postedBy = req.body.postedBy;
            post.createdOn = Date();
            posts_api.createPost(post,function(err,result){
            	if(err) {res.send(err);}
            	res.json(result);
            })
})


/**
 * @api {put} post/like/:postid like a post
 * @apiName LikePost
 * @apiGroup Post
 *
 * @apiParam {Number} id Users unique ID.
 * @apiParam {Number} id Posts unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : false
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Post not found"
 *     }
 */






router.put('/post/like/:postid',function(req,res){
	var entry=req.body.user_id;
	posts_api.likePost(req.params.postid,entry,function(err,result){
		if(err) {res.send(err);}
		console.log('response of like is',result);
		res.json(result);
	})
})


/**
 * @api {put} post/unlike/:postid Unlike a post
 * @apiName UnlikePost
 * @apiGroup Post
 *
 * @apiParam {Number} id Users unique ID.
 * @apiParam {Number} id Posts unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : false
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Post not found"
 *     }
 */




router.put('/post/unlike/:postid',function(req,res){
	var entry=req.body.user_id;
	posts_api.unlikePost(req.params.postid,entry,function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})	
});

/**
 * @api {put} post/flag/:postid Flag a post
 * @apiName FlagPost
 * @apiGroup Post
 *
 * @apiParam {Number} id Users unique ID.
 * @apiParam {Number} id Posts unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : false
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Post not found"
 *     }
 */




router.put('/post/flag/:postid',function(req,res){
	var entry=req.body.user_id;
	posts_api.flagPost(req.params.postid,entry,function(err,result){
		if(err) {res.send(err);}
		res.send(result);
	})
})


/**
 * @api {put} post/unflag/:postid Unflag a post
 * @apiName UnflagPost
 * @apiGroup Post
 *
 * @apiParam {Number} id Users unique ID.
 * @apiParam {Number} id Posts unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : false
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Post not found"
 *     }
 */





router.put('/post/unflag/:postid',function(req,res){
	var entry=req.body.user_id;
	posts_api.unflagPost(req.params.postid,entry,function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})	
});


/**
 * @api {get} timeline Timeline posts
 * @apiName TimelinePost
 * @apiGroup Post
 *
 * @apiParam {Number} id posts unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : false
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Users post not found"
 *     }
 */




router.get('/timeline',function(req,res){
	// console.log('the id is',req.body.id);
	var user = {};
	user.user_id = req.user._id;
	var data ={};
	posts_api.findPost(user,data,function(err,result){
		if(err){res.send(err);}
		res.json(result);
	})
})


/**
 * @api {delete} deletepost/:id Delete post
 * @apiName DeletePost
 * @apiGroup Post
 *
 * @apiParam {Number} id posts unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		 result: { ok: 1, n: 1 }
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Post not found"
 *     }
 */





router.delete('/deletepost/:id',function(req,res){
	// console.log('body is',req.body);
	var post = {};
	post._id = req.params.id;
	posts_api.removePost(post,function(err,result){
		if(err){res.send(err);}
		console.log('result is',result);
		res.json(result);
	})
})


/**
 * @api {put} featurepost/:id Feature a post
 * @apiName FeaturePost
 * @apiGroup Post
 *
 * @apiParam {Number} id Posts unique ID.
 * @apiParam {String} Featured Post feature.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : true
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Post not found"
 *     }
 */





router.put("/featurepost/:id",function(req,res){
	console.log('feature post');
	var post = {};
	post._id = req.params.id;
	var updatedata = {};
	updatedata.featured = true;
	posts_api.updatePost(post,updatedata,function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})
});


/**
 * @api {put} unfeaturepost/:id Unfeature a post
 * @apiName UnfeaturePost
 * @apiGroup Post
 *
 * @apiParam {Number} id Posts unique ID.
 * @apiParam {String} Featured Post unfeature.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : false
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "Post not found"
 *     }
 */





router.put("/unfeaturepost/:id",function(req,res){
	console.log('unfeature post');
	var post = {};
	post._id = req.params.id;
	var updatedata = {};
	updatedata.featured = false;
	posts_api.updatePost(post,updatedata,function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})	
});


/**
 * @api {get} featuredPosts Get featured Posts
 * @apiName FeaturedPosts
 * @apiGroup Post
 *
 * @apiParam {String} Featured Post feature.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 * 		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : true
 *	}
 *	{
 *		"_id" : ObjectId("577769c8bcdab8a5223c6395"),
 *		"createdOn" : ISODate("2016-07-02T07:14:16Z"),
 *		"postedBy" : "akshay.rao@daffodilsw.com",
 *		"catType" : "quotes",
 *		"img" : "/images/Img-1661310859490186.jpeg",
 *		"title" : "hello",
 *		"user_id" : "57711864bb9456b373f0956b",
 *		"featured" : true,
 *		"flagBy" : [
 *			"57711864bb9456b373f0956b"
 *		],
 *		"__v" : 0,
 *		"likeCount" : 0,
 *		"likeBy" : [ ],
 *		"commentCount" : 1,
 *		"comments" : [
 *			{
 *				"commentedOn" : "577769c8bcdab8a5223c6395",
 *				"comment" : "Istanbul :)",
 *				"creatorName" : "akshay.rao@daffodilsw.com",
 *				"creatorId" : "57711864bb9456b373f0956b",
 *				"_id" : ObjectId("577e1b8d74e70df6284c1c9f"),
 *				"replyData" : [ ]
 *			}
 *		]
 *	}
 * 
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No Post found"
 *     }
 */






router.get('/featuredPosts',function(req,res){
	var post = {};
	var data = {};
	post.featured = true;
	posts_api.findPost(post,data,function(err,result){
		if(err){res.send(err);}
		res.json(result);
	})	
})


/**
 * @api {get} featuredPosts/:categoryName Featured posts by category
 * @apiName CategoryFeaturedPost
 * @apiGroup Post
 *
 * @apiParam {String} category Posts Category.
 * @apiParam {String} Featured Post unfeature.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571a0a924f59dc2a0e53ce88"),
 *		"createdOn" : ISODate("2016-04-22T11:27:14Z"),
 *		"postedBy" : "akshayrao571@gmail.com",
 *		"catType" : "others",
 *		"img" : "/images/Img-1269243359565734.jpeg",
 *		"title" : "dgshjb",
 *		"user_id" : "5711e8e3cba799dc2fc88f65",
 *		"flagBy" : [ ],
 *		"likeBy" : [
 *			"571348b336196208545ec2d9"
 *		],
 *		"comments" : [ ],
 *		"__v" : 0,
 *		"likeCount" : 1,
 *		"featured" : true
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No Post found"
 *     }
 */





router.get('/featuredPosts/:categoryName',function(req,res){
	var post = {};
	var data ={};
	post.featured = true;
	post.catType = req.params.categoryName;
	posts_api.findPost(post,data,function(err,result){
		if(err){res.send(err);}
		res.json(result);
	})	
})


/**
 * @api {post} flaggedposts Flagged posts
 * @apiName FlaggedPost
 * @apiGroup Post
 *
 * @apiParam {String} category Posts Category.
 * @apiParam {String} Featured Post unfeature.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("5787135babeb53a40d91e8cd"),
 *		"user_id" : "57711864bb9456b373f0956b",
 *		"title" : "new post",
 *		"img" : "/uploads/post/london.jpg",
 *		"catType" : "london",
 *		"postedBy" : "akshay.rao@daffodilsw.com",
 *		"createdOn" : ISODate("2016-07-14T04:21:47Z"),
 *		"featured" : false,
 *		"flagBy" : [
 *			"57711864bb9456b373f0956b"
 *		],
 *		"likeBy" : [ ],
 *		"comments" : [ ],
 *		"__v" : 0
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No Post found"
 *     }
 */




router.post('/flaggedposts',function(req,res){
	console.log('the req body of flagged posts is',req.body);
	var post ={};
	post.flagBy = {$exists:true,$ne:[]};
	if(req.body.userid){
		post.user_id = req.body.userid;
	}
	var data ={};

	posts_api.findPost(post,data,function(err,result){
		if(err) {res.send(err);}
		console.log('the result here is',result);
		res.json(result);
	})
})

module.exports =router;