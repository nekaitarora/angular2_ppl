var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var postSchema = new Schema({
	postedBy:String,
	title:String,
	user_id:String,
	creatorImage:String,
	createdOn:Date,
	catType:String,
	img: String,
	comments:[{
		creatorId:String,
		creatorName:String,
		comment:String,
		commentedOn:String,
		replyData: [{
			reply:String,
			creatorName: String,
			creatorId: String
		}]
	}],
	commentCount:Number,
	likeBy:[],
	postid:String,
	likeCount:Number,
	flagCount:Number,
	flagBy:[],
	featured : { type: Boolean, default: false }
},{collection:"post"});

module.exports = mongoose.model("post",postSchema);