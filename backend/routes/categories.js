
var express = require('express'),
	router = express.Router(),
	categories_db = require('../libs/categories/categories_db'),
	categories_api = require('../libs/categories/categories_api'),
	mail = require('../config/mail')
	passport = require('passport'),
	fs = require('fs');


/**
 * @api {get} categories Get all categories
 * @apiName AllCategories
 * @apiGroup Category
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571dd4a28d901ebd1ab99221"),
 *		"img" : "/images/Img-2351696246769279.jpeg",
 *		"categories" : "quotes",
 *		"__v" : 0
 *	}
 *	{
 *		"_id" : ObjectId("571dd5ab8d901ebd1ab99222"),
 *		"img" : "/images/Img-4672146360389888.jpeg",
 *		"categories" : "wallpapers",
 *		"__v" : 0
 *	}
 *	{
 *		"_id" : ObjectId("57776144f5f7eeed19749031"),
 *		"img" : "/images/Img-929281897842884.jpeg",
 *		"categories" : "london",
 *		"__v" : 0
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No Category found"
 *     }
 */




router.get("/categories",function(req,res){
	categories_api.getCategories(function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})
})


/**
 * @api {post} addCategories Add new category
 * @apiName AddCategory
 * @apiGroup Category
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571dd4a28d901ebd1ab99221"),
 *		"img" : "/images/Img-2351696246769279.jpeg",
 *		"categories" : "quotes",
 *		"__v" : 0
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No post found"
 *     }
 */





router.post("/addCategories",function(req,res){
	var category = {};
	category.categories=[req.body.type];
	category.img=req.body.path;
	categories_api.createCategory(category,function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})
});


/**
 * @api {delete} deleteCategories/:id Delete category
 * @apiName DeleteCategory
 * @apiGroup Category
 *
 * @apiParam {Number} id    category unique id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571dd4a28d901ebd1ab99221"),
 *		"img" : "/images/Img-2351696246769279.jpeg",
 *		"categories" : "quotes",
 *		"__v" : 0
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No category found"
 *     }
 */





router.delete('/deleteCategories/:id',function(req,res){
	console.log("the cat to be deleted is",req.body);
	categories_api.deleteCategory(req.params.id,function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})
})


/**
 * @api {put} updatecat/:id Update category
 * @apiName UpdateCategory
 * @apiGroup Category
 *
 * @apiParam {Number} id    category unique id.
 * @apiParam {String} type    category type.
 * @apiParam {String} image    category Image.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("571dd4a28d901ebd1ab99221"),
 *		"img" : "/images/Img-2351696246769279.jpeg",
 *		"categories" : "quotes",
 *		"__v" : 0
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No category found"
 *     }
 */




router.put("/updatecat/:id",function(req,res){
	var category = {};
	category.categories = req.body.type;
	if(req.body.path){
		category.img = req.body.path;
	}
	console.log('the data of category is',category);
	categories_api.updateCategory(req.params.id,category,function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})
});

module.exports =router;