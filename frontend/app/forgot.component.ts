import { Component, OnInit } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';
import { Service } from './service';

@Component({
	selector: 'forgot',
	templateUrl: "../assets/forgot.html",
  	directives: [ROUTER_DIRECTIVES],	
})	

export class forgotComponent  implements OnInit {
public data;
public error;

constructor(private router: Router,private service:Service){
	this.data ={};
}	

ngOnInit() {
  if(localStorage.getItem('user') !== null){
    this.router.navigate(['Dashboard']);    
  }
}

resetPassword() {
  let url = `${'forgotpassword'}/${this.data.email}`;  	
	this.service.post(url,this.data)
	.then((res) => {
		console.log('the response user is as',res);
	})
	.catch(error => this.error = error);
}

}