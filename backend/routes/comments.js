var express = require('express'),
	router = express.Router(),
	posts_db = require('../libs/posts/posts_db'),
	posts_api = require('../libs/posts/posts_api'),
	mail = require('../config/mail')
	passport = require('passport'),
	fs = require('fs');


/**
 * @api {get} getComment/:id Get comment for post
 * @apiName CommentForPost
 * @apiGroup Comment
 *
 * @apiParam {Number} id    Posts unique id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("577769c8bcdab8a5223c6395"),
 *		"createdOn" : ISODate("2016-07-02T07:14:16Z"),
 *		"postedBy" : "akshay.rao@daffodilsw.com",
 *		"catType" : "quotes",
 *		"img" : "/images/Img-1661310859490186.jpeg",
 *		"title" : "hello",
 *		"user_id" : "57711864bb9456b373f0956b",
 *		"featured" : true,
 *		"flagBy" : [
 *			"57711864bb9456b373f0956b"
 *		],
 *		"__v" : 0,
 *		"likeCount" : 0,
 *		"likeBy" : [ ],
 *		"commentCount" : 1,
 *		"comments" : [
 *			{
 *				"commentedOn" : "577769c8bcdab8a5223c6395",
 *				"comment" : "Istanbul :)",
 *				"creatorName" : "akshay.rao@daffodilsw.com",
 *				"creatorId" : "57711864bb9456b373f0956b",
 *				"_id" : ObjectId("577e1b8d74e70df6284c1c9f"),
 *				"replyData" : [ ]
 *			}
 *		]
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No post found"
 *     }
 */




router.get('/getComment/:id',function(req,res){
	var post = {};
	var data = {};
	post._id = req.params.id;
	posts_api.findPost(post,data,function(err,result){
		if(err) {res.send(err);}
		res.json(result[0].comments);
	})
})


/**
 * @api {get} singlepost/:id Get SinglePost
 * @apiName SinglePost
 * @apiGroup Comment
 *
 * @apiParam {Number} id    Posts unique id.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("577769c8bcdab8a5223c6395"),
 *		"createdOn" : ISODate("2016-07-02T07:14:16Z"),
 *		"postedBy" : "akshay.rao@daffodilsw.com",
 *		"catType" : "quotes",
 *		"img" : "/images/Img-1661310859490186.jpeg",
 *		"title" : "hello",
 *		"user_id" : "57711864bb9456b373f0956b",
 *		"featured" : true,
 *		"flagBy" : [
 *			"57711864bb9456b373f0956b"
 *		],
 *		"__v" : 0,
 *		"likeCount" : 0,
 *		"likeBy" : [ ],
 *		"commentCount" : 1,
 *		"comments" : [
 *			{
 *				"commentedOn" : "577769c8bcdab8a5223c6395",
 *				"comment" : "Istanbul :)",
 *				"creatorName" : "akshay.rao@daffodilsw.com",
 *				"creatorId" : "57711864bb9456b373f0956b",
 *				"_id" : ObjectId("577e1b8d74e70df6284c1c9f"),
 *				"replyData" : [ ]
 *			}
 *		]
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No post found"
 *     }
 */





router.get('/singlepost/:id',function(req,res){
	// console.log('sahsa',req.body)
	var post = {};
	var data = {};
	post._id = req.params.id;
	posts_api.findPost(post,data,function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})
})


/**
 * @api {post} submitcomment Submit Comment
 * @apiName SubmitComment
 * @apiGroup Comment
 *
 * @apiParam {Number} id    Posts unique id.
 * @apiParam {String} comment    comment By the user .
 * @apiParam {String} creatorName    Name of the user who commented.
 * @apiParam {Number} commentedOn    id of post on which comment is done.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("577769c8bcdab8a5223c6395"),
 *		"createdOn" : ISODate("2016-07-02T07:14:16Z"),
 *		"postedBy" : "akshay.rao@daffodilsw.com",
 *		"catType" : "quotes",
 *		"img" : "/images/Img-1661310859490186.jpeg",
 *		"title" : "hello",
 *		"user_id" : "57711864bb9456b373f0956b",
 *		"featured" : true,
 *		"flagBy" : [
 *			"57711864bb9456b373f0956b"
 *		],
 *		"__v" : 0,
 *		"likeCount" : 0,
 *		"likeBy" : [ ],
 *		"commentCount" : 1,
 *		"comments" : [
 *			{
 *				"commentedOn" : "577769c8bcdab8a5223c6395",
 *				"comment" : "Istanbul :)",
 *				"creatorName" : "akshay.rao@daffodilsw.com",
 *				"creatorId" : "57711864bb9456b373f0956b",
 *				"_id" : ObjectId("577e1b8d74e70df6284c1c9f"),
 *				"replyData" : [ ]
 *			}
 *		]
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No post found"
 *     }
 */






router.post('/submitcomment',function(req,res){
	var post = {};
	post._id = req.body.commentedOn;
	var commentData = {};
	commentData.creatorId = req.body.creatorId;
	commentData.creatorName = req.body.creatorName;
	commentData.comment = req.body.comment;
	commentData.commentedOn = req.body.commentedOn;
	posts_api.updateComments(post,commentData,function(err,result){
		if(err) {res.send(err);}
		res.json(result);
	})
})


/**
 * @api {get} submitReply Submit Reply to Comment
 * @apiName SubmitReply
 * @apiGroup Comment
 *
 * @apiParam {Number} id    Posts unique id.
 * @apiParam {String} reply   Reply By the user .
 * @apiParam {String} creatorName    Name of the user who commented.
 * @apiParam {Number} creatorid    unique id of user.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *	{
 *		"_id" : ObjectId("577769c8bcdab8a5223c6395"),
 *		"createdOn" : ISODate("2016-07-02T07:14:16Z"),
 *		"postedBy" : "akshay.rao@daffodilsw.com",
 *		"catType" : "quotes",
 *		"img" : "/images/Img-1661310859490186.jpeg",
 *		"title" : "hello",
 *		"user_id" : "57711864bb9456b373f0956b",
 *		"featured" : true,
 *		"flagBy" : [
 *			"57711864bb9456b373f0956b"
 *		],
 *		"__v" : 0,
 *		"likeCount" : 0,
 *		"likeBy" : [ ],
 *		"commentCount" : 1,
 *		"comments" : [
 *			{
 *				"replyData" : [
 *					{
 *						"_id" : ObjectId("5787310e7d1141b11b9f48c9"),
 *						"creatorId" : "57711864bb9456b373f0956b",
 *						"creatorName" : "akshay.rao@daffodilsw.com",
 *						"reply" : "love Istanbul <3"
 *					}
 *				],
 *				"_id" : ObjectId("577e1b8d74e70df6284c1c9f"),
 *				"creatorId" : "57711864bb9456b373f0956b",
 *				"creatorName" : "akshay.rao@daffodilsw.com",
 *				"comment" : "Istanbul :)",
 *				"commentedOn" : "577769c8bcdab8a5223c6395"
 *			}
 *		]
 *	}
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "No post found"
 *     }
 */







router.post('/submitReply',function(req,res){
	var post ={};
	var data = {};
	console.log('the data in body is',req.body);
	post._id = req.body.postid;
	posts_api.findPost(post,data,function(err,result){
		if(err) { res.send(err);}
		var comments = result[0].comments;
		var replydata = {};
		replydata.reply = req.body.reply;
		replydata.creatorName = req.body.creatorName;
		replydata.creatorId = req.body.creatorId;
		post.index = req.body.index;
		comments[req.body.index].replyData.push(replydata);
		console.log('the comments are ',comments[req.body.index]);
		posts_api.updateReply(post,comments[req.body.index],function(error,data){
			if(error) { res.send(error);}
			res.json(data);
		})
	})
})

module.exports =router;