import { Component, OnInit } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';
import {loginComponent} from './login.component';
import { NgForm }    from '@angular/common';
import { Service } from './service';


export class Data {
	id: Number;
	username: String;
	password: String;
	email: String;
	first_name: String;
	last_name: String;
}

@Component({
	selector: 'signup',
	templateUrl: "../assets/signup.html",
  	directives: [ROUTER_DIRECTIVES],	
	// providers: [ROUTER_PROVIDERS]	
})	

export class signupComponent implements OnInit {
public data;
public error;
public terms = false;
constructor(private http:Http, private router: Router,private service: Service){
	this.data = {};
}	

ngOnInit() {
  if(localStorage.getItem('user') !== null){
    this.router.navigate(['Dashboard']);    
  }
}

agreeTerms() {
  console.log('the terms checkbox is',!this.terms);
}

register() {
  
  if(this.terms) {
    console.log('terms is checked');
    this.service.post('register',this.data)
    .then((res) => {
      console.log('res in register is',res);
      if(res){
        alert('Please check your mail to verify yourself');
        this.router.navigate(['Login']);
      }
    })
    .catch(error => this.error = error);
  }

  else if(!this.terms) {
    console.log('terms is unckecked');
    alert('Accept the term and conditions to signup');
  }
}

// register(): Promise<Data> {
//     let headers = new Headers({
//       'Content-Type': 'application/json'});	
//       return this.http.post('http://localhost:4040/register',JSON.stringify(this.data), {headers: headers})
//       .toPromise()
//       .then((res) => {
//         console.log('the response here is',res.json());
//       if(res.json().length > 0){        
//       this.router.navigate(['Dashboard']);        
//         return res.json();
//       }
//       })
//       .catch(this.handleError);
// }

//   private handleError(error: any) {
//     console.error('An error occurred', error);
//     return Promise.reject(error.message || error);
//   }

}