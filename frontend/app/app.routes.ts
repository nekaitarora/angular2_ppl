import { provideRouter, RouterConfig } from '@angular/router';

import {loginComponent} from './login.component';
import {dashboardComponent} from './dashboard.component';
import {signupComponent} from './signup.component';

export const routes: RouterConfig = [
  { path: 'login', component: loginComponent },
  { path: 'dashboard', component: dashboardComponent },
  { path: 'signup', component: signupComponent }  
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
