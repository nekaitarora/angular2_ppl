import { Component, OnInit } from '@angular/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';
import { Service } from './service';
import {FILE_UPLOAD_DIRECTIVES, FileUploader} from '../node_modules/ng2-file-upload';

@Component({
	selector: 'profile',
	templateUrl: "../assets/profile.html",
  	directives: [FILE_UPLOAD_DIRECTIVES,ROUTER_DIRECTIVES]	
})	

export class profileComponent implements OnInit {
public uploader:FileUploader = new FileUploader({url: 'http://139.162.5.142:4000/upload/profile'});	
public postdata;
public error;
public user;
public User;
public userDetail;
public editDetail = false;
public item;
public postDetail;
public path;
public flagcheck = false;
constructor(private router: Router,private service:Service){
	this.postDetail = {};
	this.userDetail = {};
	this.item = ['Male','Female','Others'];
}	

ngOnInit() {

  if(localStorage.getItem('user') == null){
    this.router.navigate(['Login']);    
  }
  this.service.list = true;  
  this.getPosts();
  this.getUserDetails();
  this.getUser();
}	

getUser() {
  this.User = localStorage.getItem('user');
  this.user = JSON.parse(this.User);	
}

getPosts() {
	this.service.get('timeline')
	.then((res) => {
		this.postdata = res;
	})
	.catch(error => this.error = error);
}

getUserDetails() {
  this.User = localStorage.getItem('user');
  this.user = JSON.parse(this.User);
	let userId = {};
	userId['id'] = this.user._id;
	this.service.post('getprofiledata',userId)
	.then((res) => {
		this.userDetail = res[0];
		console.log('user detail is',this.userDetail);
	})
	.catch(error => this.error = error);
}

editdetail() {
	this.editDetail = true;
}

submitDetail() {

  if(this.uploader.queue[0]){
  this.uploader.queue[0].upload();
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {    
        this.path = JSON.parse(response);
        console.log('the path is',this.path);
        // this.data.type = this.updateCategoryData.updatecatType;        
        this.userDetail.image = this.path;        
	console.log('the user detail is',this.userDetail);
	this.service.put('editprofile',this.userDetail)
	.then((res) => {
		this.userDetail = res;
		console.log('res of user detail is',this.userDetail);
		this.editDetail = false;
	})
	.catch(error => this.error = error);        
	}
  }
  else { 
	console.log('the user detail is',this.userDetail);
	this.service.put('editprofile',this.userDetail)
	.then((res) => {
		this.userDetail = res;
		console.log('res of user detail is',this.userDetail);
		this.editDetail = false;
	})
	.catch(error => this.error = error);
}

}

clear() {
	this.editDetail = false;	
}

comment(postData,index) {
  this.router.navigate(['SinglePost', { id: postData._id }]);
}

flag(postData,index) {
  this.postDetail.postid = postData._id;
  this.postDetail.user_id = this.user._id;
  let url = `${'post/flag'}/${this.postDetail.postid}`;
  this.putCall(url,this.postDetail,index);  
}

unflag(postData,index) {
  this.postDetail.postid = postData._id;
  this.postDetail.user_id = this.user._id;
  let url = `${'post/unflag'}/${this.postDetail.postid}`;
  this.putCall(url,this.postDetail,index);  
}


like(postData,index) {
  this.postDetail.postid = postData._id;  
  this.postDetail.user_id = this.user._id;
  let url = `${'post/like'}/${postData._id}`;
  this.putCall(url,this.postDetail,index);   
}

unlike(postData,index) {
  this.postDetail.postid = postData._id;  
  this.postDetail.user_id = this.user._id; 
  let url = `${'post/unlike'}/${postData._id}`;
  this.putCall(url,this.postDetail,index);  
}


feature(post,index) {
  let url = `${'featurepost'}/${post._id}`;  
  this.putCall(url,{},index);
}

unfeature(post,index) {
  let url = `${'unfeaturepost'}/${post._id}`;  
  this.putCall(url,{},index);
}

putCall(url,data,index) {
  this.service.put(url,data)
  .then((res) => {
    console.log(res);
    this.postdata[index] = res;   
  })
  .catch(error => this.error = error);

}

changeImage() {
	$("#imageUpload").click();
}

flaggedPosts() {

  console.log('the value of checkbox is',!this.flagcheck);

  if(!this.flagcheck){
    let data = {userid:this.user._id};
  this.service.post('flaggedposts',data)
  .then((res) => {
    console.log('res here is as',res);
    this.postdata = res;
  })
  .catch(error => this.error = error);
  }

  else {
    this.getPosts();
  }

}

}
