import { bootstrap }      from '@angular/platform-browser-dynamic';
import { HTTP_PROVIDERS } from '@angular/http';
import { ROUTER_PROVIDERS } from  '@angular/router-deprecated';
import { disableDeprecatedForms, provideForms } from '@angular/forms';
// import { LocalStorageService, LocalStorageSubscriber } from 'angular2-localStorage/LocalStorageEmitter';

import { AppComponent }   from './app.component';
import { MODAL_BROWSER_PROVIDERS } from 'angular2-modal/platform-browser/index';

bootstrap(AppComponent, [ HTTP_PROVIDERS, MODAL_BROWSER_PROVIDERS, ROUTER_PROVIDERS, disableDeprecatedForms(),provideForms() ]);
