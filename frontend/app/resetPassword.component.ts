import { Component,OnInit } from '@angular/core';
import { Service } from './service';
import { RouteParams } from '@angular/router-deprecated';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';

@Component({
	selector: 'resetPassword',
	templateUrl : `./assets/resetPassword.html`,
  directives: [ROUTER_DIRECTIVES]
})	

export class resetPasswordComponent implements OnInit{
public res;
public error;
public data;

constructor(private service:Service,private routeParams: RouteParams,private router: Router){
  this.data={};
}	

ngOnInit() {
  this.getUser();
}

getUser() {
  let id = this.routeParams.get('id');
  let token = this.routeParams.get('token');
  let url = `${'user/resetPassword'}/${token}/${id}`;
  console.log('the email and token of user is',id,token);
  this.service.get(url)
  .then((res) => {
    if(res) {
    this.res = res;
    console.log('response of user is',this.res);
  }
  else {
    alert('Sorry! This token does not exists');
    this.router.navigate(['Login']);        
  }
  })
  .catch(error => this.error = error);

}

resetPassword() {
  let id = this.routeParams.get('id');
  let token = this.routeParams.get('token');
  let url = `${'resetPassword'}/${token}/${id}`;
  console.log('the email and token of user is',id,token);
  if(this.data.newPassword == this.data.confirmPassword){
  this.service.put(url,this.data)
  .then((res) => {
  	this.res = res;
  	console.log('response of user is',this.res);
    this.router.navigate(['Login']);     
  })
  .catch(error => this.error = error);
}
else {
  alert('The passwords did not matched');
}
}

}