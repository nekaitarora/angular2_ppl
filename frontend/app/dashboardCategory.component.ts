import {Component , OnInit,ViewContainerRef} from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { FormBuilder, Validators } from '@angular/common';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router } from '@angular/router-deprecated';
import { Service } from './service';
import {FILE_UPLOAD_DIRECTIVES, FileUploader} from '../node_modules/ng2-file-upload';
import { RouteParams } from '@angular/router-deprecated';
import {Modal, BS_MODAL_PROVIDERS} from 'angular2-modal/plugins/bootstrap';

// console.log('directives',FILE_UPLOAD_DIRECTIVES);
export class Data {
  id: Number;
  name: String;
  password: String;
}

@Component({
  selector: 'dashboard',
  templateUrl : `./assets/dashboardCategory.html`,
  directives: [FILE_UPLOAD_DIRECTIVES,ROUTER_DIRECTIVES]
})

export class dashboardCategoryComponent implements OnInit{
public uploader:FileUploader = new FileUploader({url: 'http://localhost:4040/upload'});
public uploader1:FileUploader = new FileUploader({url: 'http://localhost:4040/upload'});
public uploader2:FileUploader = new FileUploader({url: 'http://localhost:4040/upload'});
public response;
public error;
public res;
public path;
public category;
public newcategory;
public data;
public elements;
public postedBy;
public user;
public User;
public upload = false;
public categoryAdd = false;
public categoryUpdate = false;
public postdata;
public updateCategoryData;
public featuredPosts;
public categoryName;
constructor(private http: Http,private router: Router,private service:Service,private routeParams: RouteParams,public modal: Modal,viewContainer: ViewContainerRef) { 
  this.data = {};
  this.postdata = {};
  this.newcategory = {};
  this.updateCategoryData = {};
  this.featuredPosts = [];  
}

ngOnInit() {

  // if(localStorage.getItem('user') == null){
  //   this.router.navigate(['Login']);    
  // }
  this.categoryName = this.routeParams.get('category');
  this.getAllCategories();
  this.getAllPosts();
  this.getFeauteredPosts();
  this.getUser();

}

getUser() {
  if(localStorage.getItem('user') !== null){
  this.service.list = true;
  this.User = localStorage.getItem('user');
  console.log('user here is',this.User);
  this.user = JSON.parse(this.User);
  this.data.postedBy = this.user.email;
}
}

getFeauteredPosts() {
  let url = `${'featuredPosts'}/${this.categoryName}`;      
  this.service.get(url)
  .then((res) => {
    console.log('featured posts are',res);
    this.featuredPosts = res;
  })
  .catch(error => this.error = error);
}

getAllCategories() {
  this.service.get('categories')
  .then((res) => {
    this.category = res;
  })
  .catch(error => this.error = error);
}

getAllPosts() {
  let url = `${'dashboard'}/${this.categoryName}`;    
  this.service.get(url)
  .then((res) => {
    this.elements = res;
    console.log('the elements data is',this.elements);
  })
  .catch(error => this.error = error);
}

post() {
  console.log('the image to be uploaded is',this.uploader);
  this.uploader.queue[0].upload();
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('response', JSON.parse(response));// the url will be in the response
        this.path = JSON.parse(response);
        this.data.path = this.path.path;
        this.data.id = this.user._id;
          this.service.post('post',this.data)
            .then((res) => {
              console.log('res is as',res);
              this.elements = res;
            })
            .catch(error => this.error = error);
    };
}

addNewCategory(newcategory) {
  console.log('image to be uploaded is',this.uploader1);
  this.uploader1.queue[0].upload();
    this.uploader1.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('response', JSON.parse(response));// the url will be in the response
        this.path = JSON.parse(response);
        this.data.type = newcategory.categoryType;
        this.data.path = this.path.path;
          this.service.post('addCategories',this.data)
            .then((res) => {
              console.log('res is as',res);
              this.categoryAdd = false;
              this.getAllCategories();
            })
            .catch(error => this.error = error);
    };  
}

updateCategory(category,index) {
  this.categoryUpdate = true;
  this.upload = false;  
  this.categoryAdd = false;  
  console.log('the cat to be updated is',category,index);
  this.updateCategoryData.updatecatType = category.categories;
  this.updateCategoryData.path = category.img;
  this.updateCategoryData._id = category._id;
}

updateCategories() {
  if(this.uploader2.queue[0]){
  this.uploader2.queue[0].upload();
    this.uploader2.onCompleteItem = (item: any, response: any, status: any, headers: any) => {    
        this.path = JSON.parse(response);
        this.data.type = this.updateCategoryData.updatecatType;        
        this.data.path = this.path.path;        
}
}
else {
    this.data.type = this.updateCategoryData.updatecatType;        
}

  let url = `${'updatecat'}/${this.updateCategoryData._id}`;  
  this.service.put(url,this.data)
  .then((res) => {
    console.log('the res after updation is',res);
  })
  .catch(error => this.error = error);
}

delete(post,index) {
  console.log('in the delete post');
  let url = `${'deletepost'}/${post._id}`;  
  this.service.delete(url)
  .then((res) => {
    this.elements.splice(index,1);
  })
  .catch(error => this.error = error);
}

deleteCategory(category,index) {
  console.log('category here is',category,index);
  let url = `${'deleteCategories'}/${category._id}`;  
  this.service.delete(url)
  .then((res) => {
    this.category.splice(index,1);
  })
  .catch(error => this.error = error);
}

comment(postData,index) {
  this.router.navigate(['SinglePost', { id: postData._id }]);
}

flag(postData,index) {
  let url = `${'post/flag'}/${postData._id}`;
  this.postdata.postid = postData._id;
  this.postdata.user_id = this.user._id;
  this.putCall(url,this.postdata,index);  
}

unflag(postData,index) {
  let url = `${'post/unflag'}/${postData._id}`;
  this.postdata.postid = postData._id;
  this.postdata.user_id = this.user._id;
  this.putCall(url,this.postdata,index);  
}


like(postData,index) {
  let url = `${'post/like'}/${postData._id}`;
  this.postdata.postid = postData._id;  
  this.postdata.user_id = this.user._id;
  this.putCall(url,this.postdata,index);   
}

unlike(postData,index) {
  let url = `${'post/unlike'}/${postData._id}`;
  this.postdata.postid = postData._id;  
  this.postdata.user_id = this.user._id; 
  this.putCall(url,this.postdata,index);  
}


feature(post,index) {
  let url = `${'featurepost'}/${post._id}`;  
  this.putCall(url,{},index);
}

unfeature(post,index) {
  let url = `${'unfeaturepost'}/${post._id}`;  
  this.putCall(url,{},index);
  // this.getFeauteredPosts();
}

putCall(url,data,index) {
  this.service.put(url,data)
  .then((res) => {
    console.log(res);
    this.elements[index] = res;   
    this.getFeauteredPosts();
  })
  .catch(error => this.error = error);

}

categoryPosts(category) {
  console.log('the selected category is',category);
  this.router.navigate(['DashboardCategory' , {category:category.categories}]);
}



uploadpost() {
  if(this.user){  
  this.upload = true;
  this.categoryAdd = false;
  this.categoryUpdate = false;
  console.log('this is upload post');
}
else{
  // alert('Log in to upload a post');
        return this.modal.alert()
                .size('md')
                .isBlocking(true)
                .showClose(true)
                .title('You must be logged in to upload post')
                .open()
                .catch(err => alert("ERROR"))
                .then(dialog => dialog.result)
                .then(result => { 
                  });   
}
}

clear() {
  this.upload = false;
  console.log('clear click call');
}

hideCategoryAdd() {
  this.categoryAdd = false;
}

addCategory() {
  this.upload = false;  
  this.categoryAdd = true;
  this.categoryUpdate = false;  
}

hideCategoryUpdate() {
  this.categoryUpdate = false;
}

unknownFlag() {
  // alert('You must be logged in to flag this post');
        return this.modal.alert()
                .size('md')
                .isBlocking(true)
                .showClose(true)
                .title('You must be logged in to flag this post')
                .open()
                .catch(err => alert("ERROR"))
                .then(dialog => dialog.result)
                .then(result => { 
                  });

}

unknownLike() {
  // alert('You must be logged in to like this post');
        return this.modal.alert()
                .size('md')
                .isBlocking(true)
                .showClose(true)
                .title('You must be logged in to like this post')
                .open()
                .catch(err => alert("ERROR"))
                .then(dialog => dialog.result)
                .then(result => { 
                  });  
}

// getAllCategories(): Promise<Data> {
//   return this.http.get("http://localhost:4040/categories")
//   .toPromise()
//   .then((res) => {
//     this.category = res.json();
//     console.log('result here is for categories',this.category);
//     return res.json();
//   })
//   .catch(this.handleError);
// }

// getAllPosts(): Promise<Data> {
//   return this.http.get("http://localhost:4040/dashboard")
//   .toPromise()
//   .then((res) => {
//     this.elements = res.json();
//     console.log('the elements data is',this.elements);
//     return res.json();
//   })
//   .catch(this.handleError);
// }

  // private handleError(error: any) {
  //   console.error('An error occurred', error);
  //   return Promise.reject(error.message || error);
  // }

// post() {
//   console.log('lets submit the data');
// }

}
